# Stage 3 bootloader info

## Basic starting neessities
- The kernel will need to be in a known virtual memory state
- The kernel will need to be passed a list of modules for the kernel to start

## Modules
- The modules will be loaded as ELF files into somewhere in the virtual memory space.
- The kernel will be passed a table of these modules and various necessities for its virtual memory requirements
  - The VM requirements can change as the kernel changes

## Some extra misc ideas
- Luckily, we can use statically linked filesystem modules that can be reused for each implementation
  - This means that the initramfs can use different filesystems as the project progresses
  - I'd like to use the same interface that BOOTBOOT has for its filesystem driver.
  - Of course, this would make for some code duplication for the FS drivers in kernel modules, so perhaps we can use the same drivers as the kernel??
- We won't be able to use the VMM from the kernel, but I don't imagine that to be a problem, since we'll only need to set it up in a known state
