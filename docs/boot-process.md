# The boot process of DaeDream: V1

## Loading the kernel:

The kernel will contain its base parts:
- IPC
- VMM
- Some super basic hardware abstractions

1. We will initially use any sort of bootloader to load a stage 3 bootloader. This stage 3 will initialize the environment to a known state for the kernel to start in, including loading in any sort of external modules from the initramfs.
2. This stage 3 will do the following:
  - Start the kernel
  - Load up modules and tell the kernel where these modules are.
3. The kernel will start separate processes for each of these modules

## Reasoning:

### Pros:

- This avoids the necessity for any filesystem drivers in the kernel. Instead, the stage 3 will load modules into a known spot for the kernel to use
- This allows for flexibility in the bootloader, meaning the kernel can stay as small as we can make it. This stage 3 can also change as the kernel changes without needing to change the bootloaders.

### Cons:
- I'd need to create a/some stage 3 bootloader(s).
- I'd need to maintain each one as the kernel changes and necessary changes are made

### Ideas:
- Any ideas can be found in the info for the 3rd stage



## Some ideas:

- ~~Use James Molloy's initrd format for the initrd?~~
- ~~Use FAT16 perhaps? It may be easier as we can just import the fatfs driver which exists.~~
- Using USTAR

