# The Program Loader

In case you can't tell from its inclusion in the general block structure,
the program loader, although running in user space,
is extremely important to the system, and it needs to be able to do a very specific set of things
to be useful.


## What the program loader must be able to do:
- Load an init program to load the rest of the system
- Communicate with a VFS driver.


## The general gist of how the program loader should work:

1. Wait for a message to load a program
  - This message will most likely contain these things:
    - a file path for the VFS to load from
    - any arguments to be sent to the program
    - attributes to give the kernel in terms of permissions
2. Communicate with the VFS to load the program into memory
3. Tell the VMM that the memory is executable.
4. Create a process and start that program.
5. Back to step 1

### Reasoning for this method:

The reason I like this method is because it allows each recieved message to spawn another thread and all program loading is done in a very concurrent manner.
