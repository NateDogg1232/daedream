# DaeDream's Thread/Process Model

## Threads
- At its core, threads will only contain a new stack, context, and program pointer.
- This means that they will share the same page table as the original process

## Processes
- A process will have its own memory, along with all the differences that come with a thread.
- What this means in implementation is:
  - Process will have to have its own allocated memory and the memory will be given to it,
  and removed from the process that created the process.
