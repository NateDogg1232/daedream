# The VFS

The VFS (Virtual File System) is responsible for abstracting the any file systems into a simple unified structure.

## What the VFS must do:

- Abstract any file system specific details away unless needed
- Provide a consistent method for loading files
- Provide any necessary metadata about files


## What this VFS will have

The VFS will have the regular POSIX standard calls. These include:
- `open()`
- `close()`
- `read()`
- `write()`
- `readdir()`
- `finddir()`

It will also have these properties:
- Permissions:
  - These will be the standard POSIX permissions.
