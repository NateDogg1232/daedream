ARCH ?= x86_64
NAME=DaeDream

KERNDIR=kernel
KERN=$(KERNDIR)/kern.$(ARCH).elf

STAGE3DIR=stage3
STAGE3=$(STAGE3DIR)/stage3.$(ARCH).elf

INITRDDIR=initrd
INITRD=$(IMGDIR)/BOOTBOOT/INITRD

PWD=$(shell pwd)

IMGDIR=disk
TARGET=$(NAME).vhd
IMG=$(TARGET:.vhd=.img)

INFO="[34m[INFO][0m"

all: $(TARGET)

run: $(TARGET)
	qemu-system-x86_64 -smp 2 -drive if=pflash,format=raw,readonly,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd \
	-drive if=pflash,format=raw,file=build/my_uefi_vars.fd \
	-drive file=$(TARGET)

$(TARGET): $(IMG)
	@echo $(INFO) Converting image
	@qemu-img convert -f raw -O vpc $(IMG) $(TARGET)

$(IMG): $(IMGDIR) $(INITRD)
	@echo $(INFO) Making raw image
	@sh build/makedisk.sh $(IMGDIR) $(IMG) $(ARCH)

$(INITRD): $(STAGE3) $(KERN) $(INITRDDIR)
	@echo $(INFO) Making initrd...
	@cp $(STAGE3) $(INITRDDIR)/
	@cp $(KERN) $(INITRDDIR)/
	@cd $(INITRDDIR); tar -H ustar -cf $(PWD)/$(INITRD) *; cd $(PWD)

$(KERN):
	@echo $(INFO) Buiding kernel...
	@cd $(KERNDIR); make; cd $(PWD)

$(STAGE3):
	@echo $(INFO) Building stage 3
	@cd $(STAGE3DIR); make; cd $(PWD)

$(INITRDDIR):
	@mkdir $@
$(IMGDIR):
	@mkdir -p $@/BOOTBOOT

.PHONY: clean.kern
clean.kern:
	cd $(KERNDIR); make clean; cd $(PWD)

.PHONY: clean.stage3
clean.stage3:
	cd $(STAGE3DIR); make clean; cd $(PWD)

.PHONY: clean
clean: clean.kern clean.stage3
	@rm -r $(INITRDDIR)
	@rm -r $(IMGDIR)
	@rm -r $(IMG)
	@rm -r $(TARGET)
