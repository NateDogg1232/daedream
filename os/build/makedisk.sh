#!/bin/sh

diskdir=$1
imgname=$2
arch=$3

# Common useful funcitons
mkimg() {
    dd if=/dev/zero of=$imgname bs=1M count=100 status=noxfer
}

# Used regardless of what the arch is
echo "Moving the BOOTBOOT config to the disk directory"
mkdir -p $diskdir/BOOTBOOT/
cp bootboot/config $diskdir/BOOTBOOT/CONFIG


# Define your arch specific instructions here
mk_x86_64() {
    echo "Setting up disk directory" 1>/dev/stderr
    mkdir -p $diskdir/EFI/BOOT
    cp bootboot/bootboot.efi $diskdir/EFI/BOOT/BOOTX64.EFI > /dev/null
    echo "Making the image" 1>/dev/stderr
    mkimg
    echo "Partitioning the image" 1>/dev/stderr
    echo "g
    n
    1
    2048
    -0
    t
    1
    w" | fdisk $imgname > /dev/null # Partition the disk > /dev/null
    echo "Creating loop device (SUDO)" 1>/dev/stderr
    sudo losetup loop100 -P $imgname # Start up a loop device
    echo "Making file system (SUDO)" 1>/dev/stderr
    sudo mkfs.vfat /dev/loop100p1
    echo "Mounting file system (SUDO)" 1>/dev/stderr
    mkdir disktmp
    sudo mount /dev/loop100p1 disktmp -o uid=$UID
    cp -r disk/* disktmp/
    echo "Unmounting file system (SUDO)" 1>/dev/stderr
    sudo umount /dev/loop100p1
    rmdir disktmp
    echo "Removing loop device (SUDO)" 1>/dev/stderr
    sudo losetup -d /dev/loop100
}

mk_$arch > /dev/null

echo "Done creating $imgname"
