# DaeDream kernel's Makefile

CC = clang -c
LD = ld.lld

# Only set the arch if it hasn't been set before
ARCH ?= x86_64

# Where directories end up
SRC = src
ARCHSRC = arch
OBJDIR = obj
# All the source files we have
SOURCES = $(shell find $(SRC) -name '*.c')
# And convert them over to where the object files should end up
OBJ = $(patsubst $(SRC)/%,$(OBJDIR)/%,$(SOURCES:.c=.o))

TARGET = kern.$(ARCH).elf

INCLUDE=-Iinclude/ -I$(ARCHSRC)/$(ARCH)/include # -Istd-inc/

CFLAGS = -Wall -ffreestanding -nostdlib -D_ARCH_$(ARCH)_
LDFLAGS = -nostdlib -T $(ARCHSRC)/$(ARCH)/link.ld

#==================================
# Set any arch specific flags here
#==================================
ifeq ($(ARCH),x86_64)
	CFLAGS +=-mno-red-zone
endif

all: $(TARGET)

$(TARGET): $(OBJ)
	@$(LD) $(LDFLAGS) $^ -o $@

$(OBJDIR)/%.o: $(SRC)/%.c
	@# Make sure the directory the objects go into exists
	@mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) $(INCLUDE) -target $(ARCH)-elf  $^ -o $@

.PHONY: clean
clean:
	@rm $(OBJ)
	@rm -rf $(OBJDIR)
	@rm $(TARGET)
