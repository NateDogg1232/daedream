#include <dev/serial.h>
#include <bootboot/bootboot.h>
#include <arch.h>
#include <stdint.h>

// Contains the bootboot info structure
extern BOOTBOOT bootboot;

extern uint64_t mmio;
uint64_t* mmio_area = &mmio;

extern char environment; // Contains the loader's config arguments
char* env = &environment;

extern uint8_t fb; // The framebuffer
uint8_t* framebuffer = &fb;

void _start() {
    // Output to serial
    write_serial_string(COM_PORT1, "Hello world!\n");

    // Output the config to the serial console
    write_serial_string(COM_PORT1, env);

    // Halt the CPU
    halt();
}
