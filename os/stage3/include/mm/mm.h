#pragma once
/* mm.h
 * Should contain all memory management structures
 * and basic methods needed to do memory management.
 */


typedef void* phys_addr_t;
typedef void* virt_addr_t;
