#include <arch.h>

void halt() {
    asm volatile ("hlt");
    // If `hlt` doesn't work, just spinlock
    for (;;);
}
