#include <stddef.h>
#include <stdbool.h>
#include <io/portio.h>
#include <dev/serial.h>

bool serial_recieved(com_port_t port) {
    return (inb(port + 5) & 1) == 1;
}

char read_serial(com_port_t port) {
    // TODO: Make this poll much more periodically rather than just
    // spinlocking
    while (serial_recieved(port) == false);

    return inb(port);
}

bool is_serial_transmit_empty(com_port_t port) {
    return (inb(port + 5) & 0x20) != 0;
}

void write_serial(com_port_t port, char c) {
    // TODO: Make this poll much more periodically rather
    // than just spinlocking
    while (is_serial_transmit_empty(port) == false);

    outb(port, c);
}

void write_serial_string(com_port_t port, char* s) {
    int i = 0;
    while (s[i]) {
        write_serial(port, s[i]);
        i++;
    }
}
