#include <dev/serial.h>
#include <bootboot/bootboot.h>
#include <arch.h>
#include <stdint.h>

// Contains the bootboot info structure
extern BOOTBOOT bootboot;

extern uint64_t mmio;
uint64_t* mmio_area = &mmio;

extern char environment; // Contains the loader's config arguments
char* env = &environment;

extern uint8_t fb; // The framebuffer
uint8_t* framebuffer = &fb;

void _start() {
    // Output to serial
    write_serial_string(COM_PORT1, "Stage three started!\n");

    // Halt the CPU
    halt();
}
