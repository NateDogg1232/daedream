# DaeDream

Pronounced as "day dream"

## General Goals

- OPEN SOURCE!! (GPLv3 licensed)
- Microkernel
- Simple IPC Calls
- KISS (Keep It Simple, Stupid)
- Minimalistic

## TODO:
- Everything. Currently in planning stage. All documentation will be stored in the docs folder.

## FAQ

### Why GPLv3? Why not a more permissive license?

Very used to the license and using it in my projects. I considered the MIT license, but I felt it allowed my work to be taken too easily and marketed as their own.

## Why a microkernel?

I like the idea and promises of a microkernel. I realize there are other projects which could provide one for me like MINIX and RedoxOS, but I have extreme "Not Invented Here" syndrome, so I decided to make my own.

## Why DaeDream?

I frequent reading the OSDev Wiki a lot, and ran into [this article on the Dae Dreamer developer archetype](https://wiki.osdev.org/Dae_Dreamer) so I decided to give it the name to follow.

## What architectures does DaeDream support?

As of right now, only x86_64, but ARM (Specifically the RPi 3/4) is a future goal.

## Prerequesitites to building DaeDream

- Linux or some sort of UNIX environment (Just highly recommended)
  - This includes some sort of POSIX compliant shell to run `make` inside, since it uses many commands like `mkdir` and `rm`from it.
  - Because many of the tools such as `clang`, `lld`, `mkbootimg`, `make`, etc. are generally reserved for UNIX environments.
- clang and lld
  - These are the two things I use to compile and link my OS since they have cross-compilation built into its framework, making it nice and easy for me and others to build the OS without having to worry about compiling an entirely new compiler.
- [mkbootimg](https://gitlab.com/bztsrc/bootboot)
  - This is really only used if you aren't going to be booting off of the UEFI system or off of an rpi
- `tar`
  - To make the initrd
- `find`
  - Used in the Makefile
